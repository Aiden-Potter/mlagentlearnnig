﻿using System;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;
using Random = UnityEngine.Random;

public class MoveToGoalAgent : Agent
{
    [SerializeField]
    private Transform targetTransform;


    public MeshRenderer floor;
    public Material win;
    public Material lose;
    public Material ground;
    public override void OnEpisodeBegin()
    {
        transform.localPosition = new Vector3(Random.Range(-12f,1f), 7.6f, Random.Range(12,20f));
        targetTransform.localPosition = new Vector3(Random.Range(-12f,1f), 7.6f, Random.Range(12,20f));

        //floor.material = ground;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(targetTransform.localPosition);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float moveX = actions.ContinuousActions[0];
        float moveZ = actions.ContinuousActions[1];
        float moveSpeed = 1f;
        transform.localPosition += new Vector3(moveX, 0, moveZ) * Time.deltaTime * moveSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name != "goal")
        {
            SetReward(-1f);
            floor.material = lose;
            EndEpisode();
        }
        else
        {
            SetReward(1f);
            floor.material = win;
            EndEpisode();
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var act = actionsOut.ContinuousActions;
        act[0] = Input.GetAxisRaw("Horizontal");
        act[1] = Input.GetAxisRaw("Vertical");
    }
}